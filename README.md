# What is this?

This is a small Ada library to parse SML data (Simple Markup Language, mocking XML).

## Yet another markup language, seriously?

Yes, yes, I know... It is just that every "off-the-shelf" choices out there were not satisfying for some reasons, so I devised this format with the just trade-off between generality and usability (for me, at least).

## How do install it?

Just place the sources in some directory where your compiler will find it.  Maybe in a future I'll add a `.gpr` file and I'll integrate it with [alire](https://github.com/alire-project)

# What is SML?

SML data borrows some ideas from XML, but makes things a bit simpler. SML format is not as expressive as XML, but I find it is less cumbersome to create and it is powerful enough for most applications.

## The model behind SML

An SML data structure is a _sequence_ of _nodes_ (not a _tree_ like in XML, but see also later).  Every node has an _opening tag_, but **not** a _closing_ one; a node is implicitly closed when a new one is open.

A node has
* A _tag_
* Zero or more _attributes_
* A _description_ (a string of text)

Maybe the best way to introduce the SML format is by showing an example

```
[book]
title  : This is not a book
author : Maigrette
style  : fantasy

This is a book that does not exist, used just a silly example

[chapter introduction]
title : Introduction
from  : 1
to    : 15
summary : Just the Introduction

This is the Introduction of the fantasy book
#
# TODO: write less useless comments
```

In the example above
* Tags are shown in `[...]` the first word is the actual tag, the rest before ']' is just ignored. The ignored text is quite handy when searching for a specific node with `Ctrl-F`
* Attributes have the form `name : value`
* Comment lines have `#` as first non-blank character
* What remains is part of the description (e.g., "This is the Introduction of the fantasy book" is the description of the node with tag `chapter`)

### Hierarchy LaTeX-style

The example above shows that we can nevertheless have a kind of hierarchy, even if the nodes are organized in a sequence.  If you used LaTeX you can recognize this approach.  In LaTeX `\\subsection` marks the beginning of a sub-section that is included in the current `\\section`.  In LaTeX (sub-)sections have ending tag, but they ends when a new section begins. 

Instead in XML one would have
```xml
<chapter>
  <section>
  </section>
  
  <section>
     <subsection>
     </subsection>
  </section>

  <section>
  </section>
</chapter>
```

Personally, I find the "implict closing" of LaTeX much less cumbersome than the explicit one of XML, that is why I am using this approach in SML.
