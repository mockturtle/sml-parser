with Ada.Text_IO; use Ada.Text_IO;
with SML_Data.Parsers;
with SML_Data.Identifiers;
with Sml_Data.Nodes.Node_Scanners;
with Sml_Data.Nodes.Node_Lists;
with Line_Arrays;

procedure Test is
   use SML_Data;

   X : Nodes.Node_Lists.Node_List;
begin
   X := Parsers.Parse (Line_Arrays.Read ("data/prova.dat"));
   Nodes.Node_Lists.Dump (X);

   declare
      type Disney is (Pippo, Pluto, Paperino, Topolino);

      use Nodes.Node_Scanners;
      S : Node_Scanner := To_Scanner (X);

      type Tag_Image_Array is array (Disney) of Identifiers.Id;

      package S_Tag is
        new Nodes.Node_Lists.Symbolic_Tags (Disney, Tag_Image_Array);

      Errs : S_Tag.Check_Error := S_Tag.Check_Tags (X);
   begin
      for K in 1 .. S_Tag.N_Errors (Errs) loop
         Put_Line ("Bad tag '"
                   & Identifiers.To_String (S_Tag.Bad_Tag (Errs, K))
                   & "' at line "
                   & Nodes.Line_Number'Image (S_Tag.Bad_Line (Errs, K)));
      end loop;
      while not End_Of_List (S) loop
         case S_Tag.Symbolic_Tag (Peek (S)) is
            when Pippo =>
               Put_Line ("Yuk yuk");

            when Pluto =>
               Put_Line ("Woof!");

            when others =>
               Put_Line ("Unimplemented");
         end case;

         Next(S);
      end loop;
   end;
end Test;
