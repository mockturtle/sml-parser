with Ada.Strings.Unbounded.Less_Case_Insensitive;
with Ada.Strings.Unbounded.Equal_Case_Insensitive;

package body Sml_Data.Identifiers is
   use Ada.Strings.Unbounded;

   function Is_Valid_Id (X : String) return Boolean
   is (X'Length > 0
       and then (
                 Is_In (X (X'First), Id_Start_Charset)
                 and
                   (for all K of X => (Is_In (K, Id_Charset)))
                )
      );

   function To_Id (X : String) return Id
   is (Id'(To_Unbounded_String (X)));

   function To_String (X : Id) return String
   is (To_String (Unbounded_String (X)));


   function "<" (X, Y : Id) return Boolean
   is (if X /= No_Id and Y /= No_Id then
          Less_Case_Insensitive (Unbounded_String (X), Unbounded_String (Y))
       else
          raise Constraint_Error);

   function "=" (X, Y : Id) return Boolean
   is (Equal_Case_Insensitive (Unbounded_String (X), Unbounded_String (Y)));

end Sml_Data.Identifiers;
