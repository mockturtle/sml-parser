with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Indefinite_Vectors;

with Ada.Strings.Maps;
with Ada.Strings.Unbounded;
with Line_Arrays;

with Sml_Data.Identifiers;

package Sml_Data.Nodes is
   use Sml_Data.Identifiers;

   type Node_Type is tagged private
     with Constant_Indexing => Attribute_Value;

   type Attribute_Set is private;

   type Line_Number is new Positive;


   procedure Set_Attribute (Set   : in out Attribute_Set;
                            Attr  : Id;
                            Value : String);


   function Create (Node_Tag    : Id;
                    Attributes  : Attribute_Set;
                    Description : Line_Arrays.Line_Array;
                    Line        : Line_Number)
                    return Node_Type
     with Post => Tag (Create'Result) = Node_Tag;

   function Tag (Item : Node_Type) return Id;

   function Line (Item : Node_Type) return Line_Number;

   --  procedure Set_Description (Item  : in out Node_Type;
   --                             Descr : in Line_Arrays.Line_Array);
   --
   function Description (Item : Node_Type) return Line_Arrays.Line_Array;

   function Description (Item : Node_Type) return String
   is (Line_Arrays.Join (Item.Description));

   procedure Set_Attribute (Item  : in out Node_Type;
                            Attr  : Id;
                            Value : String);

   function Has_Attribute (Item  : Node_Type;
                           Attr  : Id)
                           return Boolean;

   function Attribute_Value (Item  : Node_Type;
                             Attr  : Id)
                             return String;

   function Attribute_Value (Item  : Node_Type;
                             Attr  : String)
                             return String
   is (Item.Attribute_Value (To_Id (Attr)));

   function Attribute_Value (Item    : Node_Type;
                             Attr    : Id;
                             Default : String) return String;


   generic
      type Attribute_Type is (<>);
      type Attribute_Image_Array is array (Attribute_Type) of Id;

      Attribute_Images : Attribute_Image_Array := (others => No_Id);
   package Symbolic_Attributes is

      procedure Set_Attribute (Item  : in out Node_Type;
                               Attr  : Attribute_Type;
                               Value : String);

      function Has_Attribute (Item  : Node_Type;
                              Attr  : Attribute_Type)
                              return Boolean;

      function Attribute_Value (Item  : Node_Type;
                                Attr  : Attribute_Type)
                                return String;

      function Image (X : Attribute_Type) return Id;
   end Symbolic_Attributes;


private
   use Ada.Strings.Unbounded;

   package Key_Value_Maps is
     new Ada.Containers.Indefinite_Ordered_Maps (Key_Type     => Id,
                                                 Element_Type => String);

   type Attribute_Set is
      record
         Set : Key_Value_Maps.Map;
      end record;

   type Node_Type is tagged
      record
         Tag         : Id;
         Attributes  : Key_Value_Maps.Map;
         Description : Line_Arrays.Line_Array;
         Line        : Line_Number;
      end record;

   function Tag (Item : Node_Type) return Id
   is (To_Id (To_String (Item.Tag)));

   function Line (Item : Node_Type) return Line_Number
   is (Item.Line);

   function Description (Item : Node_Type) return Line_Arrays.Line_Array
   is (Item.Description);

   function Has_Attribute (Item  : Node_Type;
                           Attr  : Id)
                           return Boolean
   is (Item.Attributes.Contains (Attr));

   function Attribute_Value (Item  : Node_Type;
                             Attr  : Id)
                             return String
   is (Item.Attributes.Element (Attr));

   function Attribute_Value (Item    : Node_Type;
                             Attr    : Id;
                             Default : String) return String
   is (if Item.Has_Attribute (Attr) then Item.Attribute_Value (Attr) else Default);

   generic
      type Symbol_Type is (<>);
      type Image_Array is array (Symbol_Type) of Id;
      Images : Image_Array;
   function Generic_Symbol_Image (Symbol : Symbol_Type)
                                  return Id;


end Sml_Data.Nodes;
