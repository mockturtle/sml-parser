
--
--  @description
--  This hierarchy of packages provides resources to parse an SML file.
--  The name SML is after XML with the 'X' replaced by 'S' for 'simple' since
--  it borrows some concepts from XML to define a format that is maybe
--  less powerful, but simpler.
--
-- An XML file is a tree of "nodes" where each node has a tag, attributes and
-- contents.  An SML file is still a collection of nodes with tags, attributes
-- and content, but it is just a list, not a tree.  This allows for a simpler
-- format with no closing tag (a node ends when a new one begins).
--
-- The following is an example of SML file
--
-- [bar]
-- name : zorro
-- label : foo
-- viva la pappa col pomodoro (content #1)
--
-- [qwerty]
-- name : goofy
-- label : bar
--
-- This is content #2
--
-- # This is a comment
--
-- Attribute lines have the form "key : value" where the key must begin
-- in column 1.  Attributes come after the header with the class name and
-- before the description.  Empty lines before or after the description
-- and spaces before and after the attribute value can be trimmed.
--
-- Lines begining with # are comments and ignored
--
-- The syntax is something like
--
--   file      := node+
--   node      := header attribute* text-line*
--   header    := ^S* '[' S* id S*  ']' S*$
--   attribute := ^id S* ':' .*$
--   text-line := does not match neither header, nor attribute
--
--

package SML_Data is


end SML_Data;
