with Ada.Containers.Indefinite_Ordered_Maps;
with Ada.Containers.Vectors;
with Ada.Strings.Bounded;
with Line_Arrays;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Strings.Maps; use Ada.Strings.Maps;

with SML_Data.Nodes.Node_Lists;
--
--  @description
--  This package provide a function to parse SML content.
package SML_Data.Parsers is


   -- What kind of trimming (removing leading or following spaces) is desired.
   -- Trimming is done in two occasions: on attribute values and on content
   -- When operating on attribute values whitespaces are removed; when
   -- operating on content empty lines (actually, with only spaces)
   -- are removed.
   type Trimming_Action is (
                            Head, -- Remove initial spaces/empty lines
                            Tail, -- Remove final spaces/empty lines
                            Both, -- Remove both initial and final spaces/empty lines
                            None  -- No trimming is done
                           );

   -- It is possible to do a minimal customization on the file syntax
   -- by defining tag markers, comment markers and attribute separators.
   -- This is done by means of the Syntax_Spec array
   type Syntax_Entity is (Tag_Open, Tag_Close, Comment_Start, Attribute_Separator);

   type Syntax_Spec is array (Syntax_Entity) of Character;

   Default_Syntax : constant Syntax_Spec := (Tag_Open            => '[',
                                             Tag_Close           => ']',
                                             Comment_Start       => '#',
                                             Attribute_Separator => ':');

   -- Parse the input given as an array of lines.  Return a list of nodes.
   function Parse (Input         : Line_Arrays.Line_Array;
                   Trimming      : Trimming_Action := Both;
                   Line_Trimming : Trimming_Action := Both;
                   Syntax        : Syntax_Spec := Default_Syntax)
                   return Nodes.Node_Lists.Node_List;

   -- Syntactic sugar for a very common case: read the input from the
   -- given file
   function Parse (Input_Filename : String;
                   Trimming       : Trimming_Action := Both;
                   Line_Trimming  : Trimming_Action := Both;
                   Syntax         : Syntax_Spec := Default_Syntax)
                   return Nodes.Node_Lists.Node_List;


end SML_Data.Parsers;
