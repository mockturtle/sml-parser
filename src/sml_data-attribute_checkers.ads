with Ada.Containers.Doubly_Linked_Lists;
with Ada.Containers.Vectors;
with Ada.Containers.Indefinite_Vectors;
with Ada.Strings.Unbounded;

with SML_Data.Nodes;
with SML_Data.Identifiers;

package Sml_Data.Attribute_Checkers is
   use SML_Data.Identifiers;

   type Attribute_Spec is private;
   type Attribute_Spec_Array is array (Positive range <>) of Attribute_Spec;

   type Attribute_Checker  is private;

   function Create (Checks : Attribute_Spec_Array) return Attribute_Checker;

   No_Check : constant Attribute_Checker;

   function Mandatory (Name : Id) return Attribute_Spec;

   function Mandatory_ID (Name : Id) return Attribute_Spec;

   function Default (Name : Id; Default : String) return Attribute_Spec;

   function Alternative (Spec : String) return Attribute_Spec;

   function Enumerative (Spec           : String;
                         Allowed_Values : String;
                         Default        : String := "";
                         Case_Sensitive : Boolean := False)
                         return Attribute_Spec;

   generic
      type Enumerative_Type is (<>);
   function Generic_Enumerative (Spec    : String;
                                 Default : String := "")
                                 return Attribute_Spec;

   function "+" (X, Y : Attribute_Checker) return Attribute_Checker;

   function "+" (X : Attribute_Spec;
                 Y : Attribute_Spec)
                 return Attribute_Checker;

   function "+" (X : Attribute_Checker;
                 Y : Attribute_Spec)
                 return Attribute_Checker;

   procedure Check (Checker : Attribute_Checker;
                    Node    : in out Nodes.Node_Type'Class);
   Missing_Mandatory     : exception;
   Missing_Alternative   : exception;
   Duplicate_Alternative : exception;
   Bad_Enumerative       : exception;
private
   use Ada.Strings.Unbounded;
   use SML_Data.Identifiers;

   package ID_Vectors is
     new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                            Element_Type => Id);

   type Check_Class is (Mandatory_ID_Class,
                        Mandatory_Class,
                        Default_Class,
                        Alternative_Class,
                        Enumerative_Class);

   type Attribute_Spec (Class : Check_Class := Mandatory_Class) is
      record
         case Class is
            when Mandatory_ID_Class | Mandatory_Class =>
               Attribute : Id;

            when Default_Class =>
               Key   : Id;
               Value : Unbounded_String;

            when Alternative_Class =>
               Alternatives : Id_Vectors.Vector;

            when Enumerative_Class =>
               Attr           : Id;
               Allowed        : ID_Vectors.Vector;
               Default        : Unbounded_String;
               Case_Sensitive : Boolean;
         end case;
      end record;

   function Mandatory (Name : id) return Attribute_Spec
   is (Attribute_Spec'(Class => Mandatory_Class,
                        Attribute => Name));

   function Mandatory_ID (Name : id) return Attribute_Spec
   is (Attribute_Spec'(Class => Mandatory_ID_Class,
                        Attribute => Name));

   function Default (Name : id; Default : String) return Attribute_Spec
   is (Attribute_Spec'(Class => Default_Class,
                        Key       => Name,
                        Value     => To_Unbounded_String (Default)));


   package Speck_lists is
     new Ada.Containers.Doubly_Linked_Lists (Attribute_Spec);

   use type Speck_Lists.List;
   subtype Speck_List is Speck_Lists.List;

   type Attribute_Checker is
      record
         Checks : Speck_List;
      end record;

   No_Check : constant Attribute_Checker := (Checks => Speck_Lists.Empty_List);



end Sml_Data.Attribute_Checkers;
