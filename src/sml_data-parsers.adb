pragma Ada_2012;
with Ada.Characters.Handling;
with Ada.Strings.Fixed;
with Ada.Strings.Maps.Constants;

with Line_Arrays.Classified;
with Line_Arrays.Regexp_Symbolic_Classifiers;
with Line_Arrays.Regexps;

with GNAT.Regpat;
with Ada.Text_IO; use Ada.Text_IO;

with Tokenize.Token_Vectors;

with SML_Data.Identifiers;
with Ada.Characters.Latin_1;


package body SML_Data.Parsers is
   use SML_Data.Identifiers;
   use Ada.Characters.Handling;

   function Chop (X : String) return String
   is (Ada.Strings.Fixed.Trim (X, Ada.Strings.Both));

   function Image (X : Integer) return String
   is (Chop (Integer'Image (X)));
   pragma Unreferenced (Image);

   function Force_Case_Maybe (X : String; Case_Sensitive : Boolean) return String
   is (if Case_Sensitive then X else  To_Lower (X));

   type Line_Class is (Header, Attribute, Text, Comment);

   type Classified_Line (Class : Line_Class) is
      record
         case Class is
            when Header =>
               Name : Id;

            when Attribute =>
               Key   : Id;
               Value : Unbounded_String;

            when Text =>
               Content : Unbounded_String;

            when Comment =>
               null;
         end case;
      end record;


   function Is_To_Be_Ignored (Line : Classified_Line) return  Boolean
   is (Line.Class = Comment);


   package SML_Classifiers is
     new Line_Arrays.Regexp_Symbolic_Classifiers
       (Line_Class      => Line_Class,
        Classified_Line => Classified_Line);

   use SML_Classifiers;

   package Classified_SML_Lines is
     new Line_Arrays.Classified
       (Classified_Line  => Classified_Line,
        Classifier_Type  => SML_Classifiers.Classifier_Type);

   function Image (Item : Classified_Line) return String
   is (Item.Class'Image &
       (case Item.Class is
           when Header    =>
              To_String (Item.Name),
           when Attribute =>
              To_String (Item.Key) & "," & To_String (Item.Value),
           when Text      =>
              To_String (Item.Content),
           when Comment   =>
              ""));

   function Extract (Item    : String;
                     Matches : Line_Arrays.Regexps.Match_Data;
                     Index   : Line_Arrays.Regexps.Fragment_Index)
                     return String
   is (Line_Arrays.Regexps.Fragment (Matches, Index))
     with Pre => Line_Arrays.Regexps.Is_Defined (Matches, Index);

   function Extract (Item    : String;
                     Matches : Line_Arrays.Regexps.Match_Data;
                     Index   : Line_Arrays.Regexps.Fragment_Index)
                     return Id
   is (To_Id (To_Lower (Extract (Item, Matches, Index))))
     with Pre => Line_Arrays.Regexps.Is_Defined (Matches, Index);

   function Extract (Item    : String;
                     Matches : Line_Arrays.Regexps.Match_Data;
                     Index   : Line_Arrays.Regexps.Fragment_Index)
                     return Unbounded_String
   is (To_Unbounded_String (Extract (Item, Matches, Index)))
     with Pre => Line_Arrays.Regexps.Is_Defined (Matches, Index);

   function Classifier_Callback (Class   : Line_Class;
                                 Item    : String;
                                 Matches : Line_Arrays.Regexps.Match_Data)
                                 return Classified_Line
   is
   begin
      case Class is
         when Header =>
            return Classified_Line'(Class   => Header,
                                    Name    => Extract (Item, Matches, 1));

         when Attribute =>
            return Classified_Line'(Class   => Attribute,
                                    Key     => Extract (Item, Matches, 1),
                                    Value   => Extract (Item, Matches, 2));

         when Text =>
            return Classified_Line'(Class   => Text,
                                    Content => Extract (Item, Matches, 0));

         when Comment =>
            return Classified_Line'(Class => Comment);
      end case;
   end Classifier_Callback;



   -----------
   -- Parse --
   -----------

   function Parse
     (Input         : Line_Arrays.Line_Array;
      Trimming      : Trimming_Action := Both;
      Line_Trimming : Trimming_Action := Both;
      Syntax        : Syntax_Spec := Default_Syntax)
      return Nodes.Node_Lists.Node_List
   is

      use Classified_SML_Lines;
      use Classified_SML_Lines.Classified_Line_Vectors;
      use Line_Arrays.Regexps;

      Result     : Nodes.Node_Lists.Node_List;

      Id_Regex   : constant String :=
                     To_Range (Identifiers.Id_Start_Charset)
                   & To_Range (Identifiers.Id_Charset) & "*";

      Open         : constant String := Quote (Syntax (Tag_Open));
      Close        : constant String := Quote (Syntax (Tag_Close));
      Comment_Head : constant String := Quote (Syntax (Comment_Start));
      Separator    : constant String := Quote (Syntax (Attribute_Separator));

      Header_Regexp    : constant String :=
                           Open
                           & Any_Space
                           & Fragment (Id_Regex)
                         & Any_String
                           & Close
                           & Any_Space;

      Comment_Regexp   : constant String := Comment_Head & Any_String;

      Attribute_Regexp : constant String :=
                           Fragment (Id_Regex)
                         & Any_Space
                           & Separator
                           & Fragment (Any_String);

      Class_Regexp     : constant Class_Regexp_Array :=
                           (Header    => R (Header_Regexp),
                            Comment   => R (Comment_Regexp),
                            Attribute => R (Attribute_Regexp),
                            Text      => Always_Match);

      Classifier       : constant Classifier_Type :=
                           Create (Regexp_Array  => Class_Regexp,
                                   Callback      => Classifier_Callback'Access);

      C_Lines : constant Classified_Line_Vector := Classify (Classifier, Input);

      Pos : Cursor := C_Lines.First;

      function To_Line (Pos : Cursor)
                        return Nodes.Line_Number
      is
      begin
         return Nodes.Line_Number (To_Index (Pos));
      end To_Line;

      ----------------------
      -- Skip_Empty_Lines --
      ----------------------

      procedure Skip_Empty_Lines (C_Lines : Vector;
                                  Pos     : in out Cursor)
      is
         function Is_Empty (X : String) return Boolean
         is (X'Length = 0 or else (for all Ch of X => Ch = ' '));

      begin
         while Pos /= No_Element
           and then C_Lines (Pos).Class = Text
           and then Is_Empty (To_String (C_Lines (Pos).Content))
         loop
            Next (Pos);
         end loop;

         --           Put_Line ("Fatto!");
      end Skip_Empty_Lines;

      ------------------
      -- Parse_Header --
      ------------------

      procedure Parse_Header (C_Lines   : Vector;
                              Pos       : in out Cursor;
                              Tag       : out Id)
      is
      begin
         Skip_Empty_Lines (C_Lines, Pos);

         if C_Lines (Pos).Class /= Header then
            raise Constraint_Error
              with "expected header found "
              & Image (C_Lines (Pos));
         end if;

         Tag := C_Lines (Pos).Name;
         Next (Pos);
      end Parse_Header;

      ----------------------
      -- Parse_Attributes --
      ----------------------

      procedure Parse_Attributes (C_Lines    : Vector;
                                  Pos        : in out Cursor;
                                  Attributes : out Nodes.Attribute_Set;
                                  Trimming   : Trimming_Action)
      is
         use Ada.Strings.Fixed;

         function Trim (X : String; How : Trimming_Action) return String
         is (case How is
                when None => X,
                when Head => Trim (X, Ada.Strings.Left),
                when Tail => Trim (X, Ada.Strings.Right),
                when Both => Trim (X, Ada.Strings.Both));
      begin
         Skip_Empty_Lines (C_Lines, Pos);

         while Pos /= No_Element and then C_Lines (Pos).Class = Attribute loop
            Nodes.Set_Attribute
              (Set   => Attributes,
               Attr  => C_Lines (Pos).Key,
               Value => Trim (To_String (C_Lines (Pos).Value), Trimming));

            Next (Pos);
         end loop;
      end Parse_Attributes;


      -----------------------
      -- Parse_Description --
      -----------------------

      procedure Parse_Description (C_Lines   : Vector;
                                   Pos       : in out Cursor;
                                   Buffer    : out Line_Arrays.Line_Array)
      is
         function Is_Empty (X : String) return Boolean

         is (X = ""
             or else
               (for all Ch of X => Is_Space (Ch)));

         procedure Trim_Head_Maybe (Buffer : out Line_Arrays.Line_Array)
         is
         begin
            if Line_Trimming = Head or Line_Trimming = Both then
               while not Buffer.Is_Empty and then  Is_Empty (Buffer.First_Element) loop
                  Buffer.Delete_First;
               end loop;
            end if;
         end Trim_Head_Maybe;

         procedure Trim_Tail_Maybe (Buffer : out Line_Arrays.Line_Array)
         is
         begin
            if Line_Trimming = Tail or Line_Trimming = Both then
               while not Buffer.Is_Empty and then Is_Empty (Buffer.Last_Element) loop
                  Buffer.Delete_Last;
               end loop;
            end if;
         end Trim_Tail_Maybe;
      begin
         while Pos /= No_Element and then C_Lines (Pos).Class = Text loop
            Buffer.Append (To_String (C_Lines (Pos).Content));
            Next (Pos);
         end loop;

         Trim_Head_Maybe (Buffer);
         Trim_Tail_Maybe (Buffer);
      end Parse_Description;

   begin
      Skip_Empty_Lines (C_Lines, Pos);

      while Pos /= No_Element loop
         declare
            use Nodes.Node_Lists;

            Tag        : Id;
            Attributes : Nodes.Attribute_Set;
            Descr      : Line_Arrays.Line_Array;
            Line       : constant Nodes.Line_Number := To_Line (Pos);
         begin
            Parse_Header (C_Lines, Pos, Tag);

            Parse_Attributes (C_Lines, Pos, Attributes, Trimming);

            Parse_Description (C_Lines, Pos, Descr);

            Append_Node (Result, Nodes.Create (Node_Tag    => Tag,
                                                     Attributes  => Attributes,
                                                     Description => Descr,
                                                     Line        => Line));
         end;
      end loop;

      return Result;
   end Parse;

   -----------
   -- Parse --
   -----------

   function Parse (Input_Filename : String;
                   Trimming       : Trimming_Action := Both;
                   Line_Trimming  : Trimming_Action := Both;
                   Syntax         : Syntax_Spec := Default_Syntax)
                   return Nodes.Node_Lists.Node_List
   is
   begin
      return Parse (Input         => Line_Arrays.Read (Input_Filename),
                    Trimming      => Trimming,
                    Line_Trimming => Line_Trimming,
                    Syntax        => Syntax);
   end Parse;


end SML_Data.Parsers;
