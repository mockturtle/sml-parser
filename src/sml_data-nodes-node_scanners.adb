package body SML_Data.Nodes.Node_Scanners is
   function To_Scanner (X : Node_Lists.Node_List) return Node_Scanner
   is (Node_Scanner'(Nodes  => X,
                     Cursor => 1));

   function End_Of_List (X : Node_Scanner) return Boolean
   is (X.Cursor > X.nodes.Last_Index);

   function Peek (X : Node_Scanner) return Node_Type
   is (X.nodes.Element (X.Cursor));


   function Peek_Tag (X : Node_Scanner) return Id
   is (Peek (X).Tag);


   ----------
   -- Next --
   ----------

   procedure Next (X : in out Node_Scanner)
   is
   begin
      if not End_Of_List (X) then
         X.Cursor := X.Cursor + 1;
      end if;
   end Next;

   ----------
   -- Prev --
   ----------

   procedure Prev (X : in out Node_Scanner)
   is
   begin
      if X.Cursor > X.Nodes.First_Index then
         X.Cursor := X.Cursor - 1;
      end if;
   end Prev;

end SML_Data.Nodes.Node_Scanners;
