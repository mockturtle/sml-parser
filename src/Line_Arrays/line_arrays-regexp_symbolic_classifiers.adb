pragma Ada_2012;
package body Line_Arrays.Regexp_Symbolic_Classifiers is


   ---------
   -- Add --
   ---------

   procedure Add (To       : in out Classifier_Type;
                  Regexp   : Regexps.Regexp_Type;
                  Class    : Line_Class)
   is
      function Default_Exists return Boolean
      is
      begin
         if To.Exprs.Is_Empty then
            return False;
         else
            return Regexps.Does_Always_Match (To.Exprs.Last_Element.Matcher);
         end if;
      end Default_Exists;
   begin
      if Regexps.Does_Always_Match (Regexp) then
         if Default_Exists then
            raise Double_Default;
         end if;

         To.Exprs.Append (Regexp_Descriptor'(Matcher    => Regexps.Always_Match,
                                             Class      => Class));
      else
         To.Exprs.Prepend (Regexp_Descriptor'(Matcher    => Regexp,
                                              Class      => Class));
      end if;
   end Add;


   ------------
   -- Create --
   ------------

   function Create (Regexp_Array : Class_Regexp_Array;
                    Callback     : Callback_Type)
                    return Classifier_Type
   is
      Result : Classifier_Type :=
                 Classifier_Type'(Exprs    => Descriptor_Lists.Empty_List,
                                  Callback => Callback);
   begin
      for Class in Line_Class loop
         Result.Add (Regexp_Array (Class), Class);
      end loop;

      return Result;
   end Create;

   --------------
   -- Classify --
   --------------

   function Classify
     (Classifier : Classifier_Type; Line : String) return Classified_Line
   is

   begin
      for Regexp of Classifier.Exprs loop
         declare
            Matched : constant Regexps.Match_Data :=
                        Regexps.Match (Pattern => Regexp.matcher,
                                       Data    => line);
         begin
            if Regexps.Is_Defined(Matched, 0)  then
               return Classifier.Callback (Regexp.Class, Line, Matched);
            end if;
         end;
      end loop;

      --
      -- If no regexp matched we raise an exception.  It makes no sense
      -- to ignore the line, since if unmatched lines are to be ignored
      -- the user can add a default entry.
      --
      raise Unmatched_Line with "Unmatched line '" & Line & "'";
   end Classify;

end Line_Arrays.Regexp_Symbolic_Classifiers;
