with Ada.Containers.Indefinite_Vectors;
--
-- This package provides function to "classify" a vector of lines.  The
-- operation is carried out by a "classifier" that is an object that
-- eats lines (represented as strings) and spit out "classified lines"
-- (that can be any non limited type).
--
-- This is useful to process line-based files, where each line is
-- separately handled.  The idea of classifier is very general.
-- A specialized, often useful, version can be found in regexp_classifiers
-- that provides a classifier based on regexps.
--
--
generic
   type Classified_Line (<>) is private;
   type Classifier_Type (<>) is private;

   with function Classify (Classifier : Classifier_Type;
                           Line       : String) return Classified_Line is <> ;

   with function Is_To_Be_Ignored (Line : Classified_Line) return  Boolean is <>;
package Line_Arrays.Classified is
   package Classified_Line_Vectors is
     new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                            Element_Type => Classified_Line);

   subtype Classified_Line_Vector is Classified_Line_Vectors.Vector;

   function Classify (Classifier : Classifier_Type;
                      Lines      : Line_Array)
                      return Classified_Line_Vectors.Vector;
end Line_Arrays.Classified;
