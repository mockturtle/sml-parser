with Ada.Containers.Doubly_Linked_Lists;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;

with Line_Arrays.Regexps;

--
-- This package provides "regexp based classifiers" to be used with the
-- generic package Line_Arrays.Classified.
--
-- Every "line class" is represented by a regular expression and the
-- classifier is built as a set of "regexp pairs" made of a regexp and
-- a callback to be called when the line matches the regexp.  The
-- callback will return the classified_line (whose type parametrizes this
-- package).  It is also possible to specify a default entry that is
-- called whenever no other regexp matches the line
--
generic
   type Line_Class is (<>);
   type Classified_Line (<>) is private;
package Line_Arrays.Regexp_Symbolic_Classifiers is
   type Callback_Type is
     access function (Class   : Line_Class;
                      Item    : String;
                      Matches : Regexps.Match_Data)
                      return Classified_Line;



   type Class_Regexp_Array is array (Line_Class) of Regexps.Regexp_Type;


   type Classifier_Type (<>) is tagged private;

   function Create (Regexp_Array : Class_Regexp_Array;
                    Callback     : Callback_Type)
                    return Classifier_Type
     with Pre =>
       (for all I in Regexp_Array'First .. Line_Class'Pred (Regexp_Array'Last) =>
          (for all J in Line_Class'Succ (I) .. Regexp_Array'Last =>
               (not (Regexps.Does_Always_Match (Regexp_Array (I)) and Regexps.Does_Always_Match (Regexp_Array (J))))
          )
       );
   --  The precondition expressed in plain English requires that at most
   --  one entry of Regexps must be a default expression


   function Classify (Classifier : Classifier_Type;
                      Line       : String) return Classified_Line;

   Double_Default : exception;
   Unmatched_Line : exception;
private
   procedure Add (To       : in out Classifier_Type;
                  Regexp   : Regexps.Regexp_Type;
                  Class    : Line_Class);

   type Regexp_Descriptor is
      record
         Matcher    : Regexps.Regexp_Type;
         Class      : Line_Class;
      end record;

   package Descriptor_Lists is
     new Ada.Containers.Doubly_Linked_Lists (Regexp_Descriptor);

   type Classifier_Type is tagged
      record
         Exprs    : Descriptor_Lists.List := Descriptor_Lists.Empty_List;
         Callback : Callback_Type;
      end record;
end Line_Arrays.Regexp_Symbolic_Classifiers;
