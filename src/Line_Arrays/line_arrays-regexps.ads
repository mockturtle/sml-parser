with Ada.Containers.Indefinite_Holders;
with Ada.Containers.Vectors;
with Ada.Strings.Maps;
private with Gnat.Regpat;


use Ada;
--
-- This package is a thin layer over GNAT.Regpat in order to have
-- a minimalistic interface and make all the other packages independent
-- on the specific Regexp implementation
--
package Line_Arrays.Regexps is
   type Undefined_Action is (Die, Use_Default);

   type Match_Data (<>) is private;

   type Fragment_Index is new Natural;

   Full_Text : constant Fragment_Index := 0;

   function It_Matched (Item : Match_Data) return Boolean;

   function Matched_Text (Item         : Match_Data;
                          On_Undefined : Undefined_Action := Die)
                          return String;

   function Fragment (Item         : Match_Data;
                      N            : Fragment_Index;
                      On_Undefined : Undefined_Action := Die)
                      return String;

   function Is_Defined (Item : Match_Data;
                        N    : Fragment_Index)
                        return Boolean;



   type Regexp_Type is private;

   Always_Match : constant Regexp_Type;

   function Does_Always_Match (X : Regexp_Type) return Boolean;

   function To_Regexp (Item : String) return Regexp_Type;

   function R (Item : String) return Regexp_Type
               renames To_Regexp;


   function Match (Pattern : Regexp_Type;
                   Data    : String)
                   return Match_Data;

   function Quote (X : String) return String;

   function Quote (C : Character) return String;

   function To_Range (Charset : Strings.Maps.Character_Set) return String;

   Any_Space  : constant String := "\s*";
   Any_String : constant String := ".*";

   function Fragment (Expr : String) return String
   is ("(" & Expr & ")");


private
   type Match_Location is record
      First : Natural := 0;
      Last  : Natural := 0;
   end record;

   No_Location : constant Match_Location := (0, 0);

   package Match_Vectors is
     new Ada.Containers.Vectors (Index_Type   => Fragment_Index,
                                 Element_Type => Match_Location);

   subtype Match_Array is Match_Vectors.Vector;

   type Match_Data (First, Last : Natural) is
      record
         Line    : String (First .. Last);
         Matches : Match_Array;
      end record;
   use type GNAT.Regpat.Pattern_Matcher;

   No_Match : constant Match_Data :=
                (First       => 2,
                 Last        => 1,
                 Line        => "",
                 Matches     => Match_Vectors.Empty_Vector);

   package Pattern_Matcher_Holders is
     new Ada.Containers.Indefinite_Holders (Gnat.Regpat.Pattern_Matcher);

   type Regexp_Type is
      record
         Matcher : Pattern_Matcher_Holders.Holder;
      end record;

   Always_Match : constant Regexp_Type :=
                    (Matcher => Pattern_Matcher_Holders.Empty_Holder);
end Line_Arrays.Regexps;
