pragma Ada_2012;
with Ada.Characters.Latin_1;
with Ada.Strings.Maps.Constants;
with Ada.Strings.Unbounded; use Ada.Strings.Unbounded;
with Ada.Text_IO; use Ada.Text_IO;
package body Line_Arrays.Regexps is
   function Undefined_Match (On_Undefined : Undefined_Action) return String
   is
   begin
      case On_Undefined is
         when Die =>
            raise Constraint_Error;

         when Use_Default =>
            return "";
      end case;
   end Undefined_Match;

   function It_Matched (Item : Match_Data) return Boolean
   is (Item /= No_Match);

   function Matched_Text (Item         : Match_Data;
                          On_Undefined : Undefined_Action := Die)
                          return String
   is
   begin
      return Fragment (Item, Full_Text, On_Undefined);
   end Matched_Text;

   function Fragment (Item         : Match_Data;
                      N            : Fragment_Index;
                      On_Undefined : Undefined_Action := Die)
                      return String
   is
   begin
      if Is_Defined (Item, N) then
         --  Put_Line ("---");
         --  Put_Line (N'Image);
         --  Put_Line (Item.Matches.Last_Index'Image);
         --  Put_Line (Item.Matches (N).First'Image);
         --  Put_Line (Item.Matches (N).Last'Image);
         --  Put_Line (Item.Line);
         return Item.Line (Item.Matches (N).First .. Item.Matches (N).Last);
      else
         return Undefined_Match (On_Undefined);
      end if;
   end Fragment;

   function Is_Defined (Item : Match_Data;
                        N    : Fragment_Index)
                        return Boolean
   is
   begin
      return not Item.Matches.Is_Empty
        and then N <= Item.Matches.Last_Index
        and then Item.Matches (N) /= No_Location;
   end Is_Defined;


   ----------------
   -- Is_Default --
   ----------------

   function Does_Always_Match (X : Regexp_Type) return Boolean is
   begin
      return X.Matcher.Is_Empty;
   end Does_Always_Match;

   ---------------
   -- To_Regexp --
   ---------------

   function To_Regexp (Item : String) return Regexp_Type is
      use Gnat.Regpat;
      use Pattern_Matcher_Holders;
   begin
      return Regexp_Type'(Matcher => To_Holder (Compile (Item)));
   end To_Regexp;

   function Match (Pattern : Regexp_Type;
                   Data    : String)
                   return Match_Data
   is
      function Export (Data  : String;
                       Match : Gnat.Regpat.Match_Array) return Match_Data
      is
         Result : Match_Data := (First       => Data'First,
                                 Last        => Data'Last,
                                 Line        => Data,
                                 Matches     => <>);
      begin
         --  Put_Line ("<" & Data & ">" & Data'First'Image & Data'Last'Image);
         for I in Match'Range loop
            --  Put_Line (I'Image & ", " & Match (Natural (I)).First'Image
            --            & Match (Natural (I)).Last'Image);
            Result.Matches.Append
              (Match_Location'(First => Match (Natural (I)).First,
                               Last  => Match (Natural (I)).Last));
         end loop;
         --  Put_Line ("<>");
         return Result;
      end Export;
   begin
      if Does_Always_Match (Pattern) then
         return Result : Match_Data := (First => Data'First,
                                        Last => Data'last,
                                        Line        => Data,
                                        Matches     => <>)
         do
            Result.Matches.Append ((First => Data'First,
                                    Last  => Data'Last));
         end return;
      else
         declare
            use Gnat.Regpat;
            Matched : Gnat.Regpat.Match_Array (0 .. Paren_Count (Pattern.Matcher.Element));
         begin
            Gnat.Regpat.Match (Self    => Pattern.Matcher.Element,
                               Data    => Data,
                               Matches => Matched);

            return Export (Data, Matched);
         end;
      end if;

   end Match;


   function Quote (X : String) return String
   is (GNAT.Regpat.Quote (X));

   function Quote (C : Character) return String
   is (Quote ((1 => C)));

   function To_Range (Charset : Strings.Maps.Character_Set) return String
   is
      use Ada.Strings.Maps;
      use Ada.Strings.Maps.Constants;

      function Quote (Char : Character) return String
        with Pre => Is_In (Char, Control_Set);

      function Quote (Char : Character) return String
      is
         use Ada.Characters;
      begin
         case Char is
            when Latin_1.LF =>
               return "\n";

            when Latin_1.CR =>
               return "\r";

            when Latin_1.HT =>
               return "\t";

            when others =>
               raise Constraint_Error
                 with "Unimplemented char N." & Integer'Image (Character'Pos (Char));
         end case;
      end Quote;

      Special        : constant Character_Set := To_Set ("\()[].*+?^") or Control_Set;
      Normal_Subset  : constant Character_Set := Charset and not Special;
      Special_Subset : constant Character_Set := Charset and Special;

      Buffer         : Unbounded_String;
   begin
      Buffer  := To_Unbounded_String (To_Sequence (Normal_Subset));

      for Char of To_Sequence (Special_Subset) loop
         if Is_In (Char, Control_Set) then
            Buffer := Buffer & Quote (Char);
         else
            Buffer := Buffer & "\" & Char;
         end if;
      end loop;

      return "[" & To_String (Buffer) & "]";
   end To_Range;

end Line_Arrays.Regexps;
