with Ada.Containers.Indefinite_Vectors;
with SML_Data.Nodes.Node_Lists;

--
-- A node scanner is an object that simplifies moving along a Node_List.
-- You could ask why not using the iteration procedure defined for lists?
-- The reason is that in many case you do not iterate over a Node_List in
-- a loop-style, but rather in a parser-style: the list is passed among
-- different procedures that "consume" the data on the list, sometimes
-- backing up.
--
-- Brief description of the scanner model
-- * A node scanner is created starting from a Node_List
-- * A node scanner has always a cursor that points to "current node"
-- or "out of the list".
-- * The cursor initially points to the first node of the list
-- * A node scanner allows you to
--   - Get the current node
--   - Move to the previous node
--   - Move to the next node
--   - Check if the cursor is out of the list
--
package SML_Data.Nodes.Node_Scanners is

   type Node_Scanner (<>) is private;

   function To_Scanner (X : Node_Lists.Node_List) return Node_Scanner;

   function End_Of_List (X : Node_Scanner) return Boolean;

   function Peek (X : Node_Scanner) return Node_Type
     with Pre => not End_Of_List (X);

   function Peek_Tag (X : Node_Scanner) return Id
     with Pre => not End_Of_List (X);
   -- Syntactic sugar, but very useful

   function Tag_Is (X : Node_Scanner; Cl : Id) return Boolean
   is ((not End_Of_List (X)) and then (Peek_Tag (X) = Cl));
   -- Syntactic sugar again, very convenient since it includes also the
   -- end-of-data case

   procedure Next (X : in out Node_Scanner);

   procedure Prev (X : in out Node_Scanner);

private
   type Node_Scanner is
      record
         Nodes  : Node_Lists.Node_List;
         Cursor : Node_Lists.List_Index;
      end record;



end SML_Data.Nodes.Node_Scanners;
