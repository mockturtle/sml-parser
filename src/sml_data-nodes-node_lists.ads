with Ada.Containers.Indefinite_Vectors;

package Sml_Data.Nodes.Node_Lists is
   subtype List_Index is Positive;

   type Node_List is tagged private
     with
       Constant_Indexing => Element;

   procedure Append_Node (List : in out Node_List;
                          Item : Node_Type);

   function Length (List : Node_List) return Ada.Containers.Count_Type;

   function First_Index (List : Node_List) return List_Index;

   function Last_Index (List : Node_List) return List_Index;

   function Element (List : Node_List;
                     Index : List_Index)
                     return Node_Type;

   function First_Element (List : Node_List) return Node_Type;

   procedure Dump (Item : Node_List);

   generic
      type Node_Tag is (<>);
      type Tag_Image_Array is array (Node_Tag) of Id;

      Tag_Images : Tag_Image_Array := (others => No_Id);
   package Symbolic_Tags is
      function Is_Valid_Tag (X : Id) return Boolean;

      function Has_Valid_Tag (Node : Node_Type) return Boolean;

      function To_Symb (X : Id) return Node_Tag
        with Pre => Is_Valid_Tag (X);

      function Symbolic_Tag (Node : Node_Type) return Node_Tag
        with Pre => Has_Valid_Tag (Node);

      type Check_Error is private;

      Ok : constant Check_Error;

      function N_Errors (Item : Check_Error) return Natural;

      function Bad_Tag (Item : Check_Error; N : Positive) return Id
        with Pre => N <= N_Errors (Item);

      function Bad_Line  (Item : Check_Error; N : Positive) return Line_Number
        with Pre => N <= N_Errors (Item);

      function Check_Tags (List : Node_List) return Check_Error;

      Ambiguous_Id : exception;
      Unknown_Tag  : exception;
   private
      type Error_Descriptor is
         record
            Bad_Tag : Id;
            Line    : Line_Number;
         end record;

      package Error_Descriptor_Vectors is
        new Ada.Containers.Indefinite_Vectors (Index_Type   => Positive,
                                               Element_Type => Error_Descriptor);

      type Check_Error is
         record
            V : Error_Descriptor_Vectors.Vector;
         end record;

      Ok : constant Check_Error := (V => Error_Descriptor_Vectors.Empty_Vector);
   end Symbolic_Tags;

private
   package Node_Vectors is
     new Ada.Containers.Indefinite_Vectors (Index_Type   => List_Index,
                                            Element_Type => Node_Type);

   type Node_List is tagged
      record
         V : Node_Vectors.Vector;
      end record;

   function Length (List : Node_List) return Ada.Containers.Count_Type
   is (List.V.Length);

   function First_Index (List : Node_List) return List_Index
   is (List.V.Last_Index);

   function Last_Index (List : Node_List) return List_Index
   is (List.V.Last_Index);

   function Element (List  : Node_List;
                     Index : List_Index)
                     return Node_Type
   is (List.V (Index));

   function First_Element (List : Node_List) return Node_Type
   is (List.V.First_Element);

end Sml_Data.Nodes.Node_Lists;
