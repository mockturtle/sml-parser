pragma Ada_2012;
with Ada.Text_IO; use Ada.Text_IO;

with Ada.Containers.Ordered_Maps;

package body Sml_Data.Nodes.Node_Lists is

   -----------------
   -- Append_Node --
   -----------------


   procedure Append_Node (List : in out Node_List;
                          Item : Node_Type)
   is
   begin
      List.V.Append (Item);
   end Append_Node;

   ----------
   -- Dump --
   ----------


   procedure Dump (Item : Node_List)
   is
      use Key_Value_Maps;
   begin
      for Node of Item.V loop
         Put_Line ("CLASS = " & To_String (Node.Tag));

         for Pos in Node.Attributes.Iterate loop
            Put_Line ("'" & To_String (Key (Pos)) & "' = '" & Element (Pos) & "'");
         end loop;

         Put_Line ("<description>");

         for Line of Node.Description loop
            Put_Line (Line);
         end loop;
         Put_Line ("</description>");
         Put_Line ("******");
      end loop;
   end Dump;

   -------------------
   -- Symbolic_Tags --
   -------------------

   package body Symbolic_Tags is
      package Tag_Maps is
        new Ada.Containers.Ordered_Maps (Key_Type     => Id,
                                         Element_Type => Node_Tag);
      function Image  is
        new Generic_Symbol_Image (Symbol_Type => Node_Tag,
                                  Image_Array => Tag_Image_Array,
                                  Images      => Tag_Images);

      Id_To_Tag : Tag_Maps.Map;

      function Is_Valid_Tag (X : Id) return Boolean
      is (Id_To_Tag.Contains (X));

      function Has_Valid_Tag (Node : Node_Type) return Boolean
      is (Id_To_Tag.Contains (Node.Tag));

      ------------------
      -- Symbolic_Tag --
      ------------------

      function To_Symb (X : Id) return Node_Tag
      is
      begin
         if not Is_Valid_Tag (X) then
            raise Unknown_Tag with To_String (X);

         else
            return Id_To_Tag (X);
         end if;
      end To_Symb;

      function Symbolic_Tag (Node : Node_Type) return Node_Tag
      is (To_Symb (Node.Tag));


      function N_Errors (Item : Check_Error) return Natural
      is (Natural (Item.V.Length));

      function Bad_Tag (Item : Check_Error; N : Positive) return Id
      is (Item.V (N).Bad_Tag);

      function Bad_Line  (Item : Check_Error; N : Positive) return Line_Number
      is (Item.V (N).Line);

      function Check_Tags (List : Node_List) return Check_Error
      is
         Result : Check_Error := (V => Error_Descriptor_Vectors.Empty_Vector);
      begin
         for Node of List.V loop
            if not Is_Valid_Tag (Tag (Node)) then
               Result.V.Append (Error_Descriptor'(Bad_Tag => Tag (Node),
                                                  Line    => Line (Node)));
            end if;
         end loop;

         return Result;
      end Check_Tags;


   begin
      for Symbol in Node_Tag loop
         if Id_To_Tag.Contains (Image (Symbol)) then
            raise Ambiguous_Id with To_String (Image (Symbol));
         end if;

         Id_To_Tag.Insert (Key      => Image (Symbol),
                           New_Item => Symbol);
      end loop;
   end Symbolic_Tags;

end Sml_Data.Nodes.Node_Lists;
