with Ada.Containers.Ordered_Maps;
with Ada.Characters.Handling;
with Ada.Strings.Maps.Constants;
with Ada.Text_IO; use Ada.Text_IO;

package body SML_Data.Nodes is
   use Ada.Strings.Maps;

   procedure Set_Attribute (Set   : in out Attribute_Set;
                            Attr  : Id;
                            Value : String)
   is
   begin
      Set.Set.Include (Attr, Value);
   end Set_Attribute;


   function Create (Node_Tag    : Id;
                    Attributes  : Attribute_Set;
                    Description : Line_Arrays.Line_Array;
                    Line        : Line_Number)
                    return Node_Type
   is
   begin
      return Node_Type'(Tag         => Node_Tag,
                        Attributes  => Attributes.Set,
                        Description => Description,
                        Line        => Line);
   end Create;

   --  procedure Set_Description (Item  : in out Node_Type;
   --                             Descr : in Line_Arrays.Line_Array)
   --  is
   --  begin
   --     Item.Description := Descr;
   --  end Set_Description;

   procedure Set_Attribute (Item  : in out Node_Type;
                            Attr  : Id;
                            Value : String)
   is
   begin
      Item.Attributes.Include (Key      => Attr,
                               New_Item => Value);
   end Set_Attribute;



   function Generic_Symbol_Image (Symbol : Symbol_Type) return Id
   is
      use Ada.Characters.Handling;
   begin
      if Images (Symbol) = No_Id then
         return To_Id (To_Lower (Symbol_Type'Image (Symbol)));
      else
         return Images (Symbol);
      end if;
   end Generic_Symbol_Image;

   package body Symbolic_Attributes is
      function Basic_Image  is
        new Generic_Symbol_Image (Symbol_Type => Attribute_Type,
                                  Image_Array => Attribute_Image_Array,
                                  Images      => Attribute_Images);

      function Image (X : Attribute_Type) return Id
                      renames Basic_Image;

      procedure Set_Attribute (Item  : in out Node_Type;
                               Attr  : Attribute_Type;
                               Value : String)
      is
      begin
         Item.Set_Attribute (Attr  => Image (Attr),
                             Value => Value);
      end Set_Attribute;

      function Has_Attribute (Item  : Node_Type;
                              Attr  : Attribute_Type)
                              return Boolean
      is (Item.Has_Attribute (Image (Attr)));

      function Attribute_Value (Item  : Node_Type;
                                Attr  : Attribute_Type)
                                return String
      is (Item.Attribute_Value (Image (Attr)));


   end Symbolic_Attributes;



end SML_Data.Nodes;
