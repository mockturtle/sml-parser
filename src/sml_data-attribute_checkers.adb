with Tokenize.Token_Vectors;
with Ada.Characters.Handling;
with Ada.Strings.Fixed;
with Ada.Strings.Unbounded;

with SML_Data.Identifiers;

package body Sml_Data.Attribute_Checkers is
   use Ada.Characters.Handling;
   use Ada.Strings.Unbounded;
   use SML_Data.Identifiers;

    function Chop (X : String) return String
   is (Ada.Strings.Fixed.Trim (X, Ada.Strings.Both));


   function Force_Case_Maybe (X : String; Case_Sensitive : Boolean) return String
   is (if Case_Sensitive then X else  To_Lower (X));

   function Enumerative (Spec           : String;
                         Allowed_Values : String;
                         Default        : String := "";
                         Case_Sensitive : Boolean := False)
                         return Attribute_Spec
   is
      use Tokenize;

      Allowed : constant Token_Vectors.Vector :=
                  Token_Vectors.To_Vector (Split (To_Be_Splitted    => Allowed_Values,
                                                  Separator         => '|',
                                                  Collate_Separator => False));

      Result : Attribute_Spec :=
                 Attribute_Spec'(Class          => Enumerative_Class,
                                  Attr           => To_Id (Spec),
                                  Allowed        => ID_Vectors.Empty_Vector,
                                  Default        => To_Unbounded_String (Default),
                                  Case_Sensitive => Case_Sensitive);
   begin
      for A of Allowed loop
         declare
            Tmp : constant String := Force_Case_Maybe (Chop (A), Case_Sensitive);
         begin
            if not Is_Valid_Id (Tmp) then
               raise Constraint_Error with "'" & A & "' is not a valid Id";
            end if;

            Result.Allowed.Append (to_id (Tmp));
         end;
      end loop;

      return Result;
   end Enumerative;


   -----------------
   -- Alternative --
   -----------------

   function Alternative (Spec : String) return Attribute_Spec
   is
      use Tokenize;

      Alternatives : constant Token_Vectors.Vector :=
                       Token_Vectors.To_Vector (Split (To_Be_Splitted    => Spec,
                                                       Separator         => '|',
                                                       Collate_Separator => False));

      Result : Attribute_Spec (Alternative_Class);
   begin
      for Alt of Alternatives loop
         if not Is_Valid_Id (Alt) then
            raise Constraint_Error with "'" & Alt & "' is not a valid Id";
         end if;

         Result.Alternatives.Append (to_id (Alt));
      end loop;

      return Result;
   end Alternative;

   -----------
   -- Check --
   -----------

   procedure Check (Checker : Attribute_Checker;
                    Node    : in out Nodes.Node_Type'Class)
   is
      function Image (Names : ID_Vectors.Vector) return String
      is
         Result : Unbounded_String;
      begin
         for X of Names loop
            Result := Result & To_String (X) & ",";
         end loop;

         return "(" & To_String (Result) & ")";
      end Image;
   begin
      for Ck of Checker.Checks loop
         case Ck.Class is
            when Mandatory_Class | Mandatory_ID_Class =>
               if not Node.Has_Attribute (Ck.Attribute) then
                  raise Missing_Mandatory
                    with To_String (Ck.Attribute);
               end if;

               if Ck.Class = Mandatory_ID_Class then
                  if not Is_Valid_ID (Node (Ck.Attribute)) then
                     raise Constraint_Error with Node (Ck.Attribute);
                  end if;
               end if;

            when Default_Class =>
               if not Node.Has_Attribute (Ck.Key) then
                  Node.Set_Attribute (Ck.Key, To_String (Ck.Value));
               end if;


            when Alternative_Class =>
               declare
                  Counter : Natural := 0;
               begin
                  for Alt of Ck.Alternatives loop
                     if Node.Has_Attribute (Alt) then
                        Counter := Counter + 1;
                     end if;
                  end loop;

                  if Counter = 0 then
                     raise Missing_Alternative with Image (Ck.Alternatives);

                  elsif Counter > 1 then
                     raise Duplicate_Alternative with Image (Ck.Alternatives);
                  end if;
               end;

            when Enumerative_Class =>
               if not Node.Has_Attribute (Ck.Attr) then
                  if Ck.Default = Null_Unbounded_String then
                     raise Missing_Mandatory with To_String (Ck.Attr);
                  else
                     Node.Set_Attribute (Ck.Attr, To_String (Ck.Default));
                  end if;
               end if;

               declare
                  Given : constant String :=
                            Force_Case_Maybe (Node (Ck.Attr), Ck.Case_Sensitive);
               begin
                  Node.Set_Attribute (Ck.Attr, Given);

                  if not Ck.Allowed.Contains (To_Id (Given)) then
                     raise Bad_Enumerative
                       with "'" & Given & "' for attribute"
                       & To_String (Ck.Attr);
                  end if;
               end;
         end case;
      end loop;
   end Check;

   -------------------------
   -- Generic_Enumerative --
   -------------------------

   function Generic_Enumerative (Spec    : String;
                                 Default : String := "")
                                 return Attribute_Spec
   is
      Buffer : Unbounded_String;
   begin
      for Val in Enumerative_Type loop
         Buffer := Buffer & Enumerative_Type'Image (Val);

         if Val < Enumerative_Type'Last then
            Buffer := Buffer & "|";
         end if;
      end loop;

      return Enumerative (Spec           => Spec,
                          Allowed_Values => To_String (Buffer),
                          Default        => Default,
                          Case_Sensitive => False);
   end Generic_Enumerative;

   function Create (Checks : Attribute_Spec_Array) return Attribute_Checker
   is
      Result : Attribute_Checker;
   begin
      for Spec of Checks loop
         Result.Checks.Append (Spec);
      end loop;

      return Result;
   end Create;

   function "+" (X, Y : Attribute_Checker) return Attribute_Checker
   is
      Result : Attribute_Checker := X;
   begin
      for pos in Y.Checks.Iterate loop
         Result.Checks.Append (Y.Checks (Pos));
      end loop;

      return Result;
   end "+";

   function "+" (X : Attribute_Spec; Y : Attribute_Spec)
                 return Attribute_Checker
   is
      Result : Attribute_Checker;
   begin
      Result.Checks.Append (X);
      Result.Checks.Append (Y);

      return Result;
   end "+";

   function "+" (X : Attribute_Checker; Y : Attribute_Spec) return Attribute_Checker
   is
      Result : Attribute_Checker := X;
   begin
      Result.Checks.Append (Y);
      return Result;
   end "+";
end Sml_Data.Attribute_Checkers;
