with Ada.Strings.Maps;
with Ada.Strings.Unbounded;
with Ada.Strings.Maps.Constants;


package Sml_Data.Identifiers is
   use Ada.Strings.Maps;

   Id_Start_Charset : constant Character_Set :=
                        Constants.Letter_Set;

   Id_Charset       : constant Character_Set :=
                        Id_Start_Charset or Constants.Decimal_Digit_Set or To_Set ("-_");

   function Is_Valid_Id (X : String) return Boolean;

   type Id is private;

   No_Id : constant Id;

   function "<" (X, Y : Id) return Boolean
     with Pre => X /= No_Id and Y /= No_Id;

   function "=" (X, Y : Id) return Boolean;

   function To_Id (X : String) return Id
     with Pre => Is_Valid_Id (X);

   function "+" (X : String) return Id
                 renames To_Id;

   function To_String (X : Id) return String
     with
       Pre => X /= No_Id,
       Post => Is_Valid_Id (To_String'Result);
private
   use Ada.Strings.Unbounded;

   type Id is new Unbounded_String;
   --  with Dynamic_Predicate =>
   --    (Unbounded_String (Id) = Null_Unbounded_String
   --     or else
   --       Is_Valid_Id (To_String (Unbounded_Stringnbounded_String (Id))));

   No_Id : constant Id := Id (Null_Unbounded_String);

end Sml_Data.Identifiers;

